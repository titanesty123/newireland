// import { createApp } from 'vue'
// import App from './App.vue'

// // import { InlineSvgPlugin } from 'vue-inline-svg'
// // const VueInputMask = require('vue-inputmask').default
//
// // import Vue plugin
// import VueSvgInlinePlugin from 'vue-svg-inline-plugin'
//
// // import polyfills for IE if you want to support it
// import 'vue-svg-inline-plugin/src/polyfills'
//
// // initialize Vue app
// const app = createApp(App)
//
// // use Vue plugin without options
// app.use(VueSvgInlinePlugin)
// createApp(App).mount('#app')

// Vue@3
import './assets/scss/style.scss'
// import basic Vue app
import { createApp } from 'vue'
import App from './App.vue'

// import Vue plugin
import VueSvgInlinePlugin from 'vue-svg-inline-plugin'

// import polyfills for IE if you want to support it
import 'vue-svg-inline-plugin/src/polyfills'

// initialize Vue app
const app = createApp(App)

// use Vue plugin without options
app.use(VueSvgInlinePlugin)

// make v-focus usable in all components
app.directive('scroll', {
  n: function (el, binding) {
    const onScrollCallback = binding.value
    window.addEventListener('scroll', () => onScrollCallback())
  }
})

// mount Vue app
app.mount('#app')
